import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError as observableThrowError } from 'rxjs';

import { Order } from '../models/Order';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private ordersUrl = 'http://localhost:8080/api/orders';

  constructor(private http: HttpClient) {}

  getOrders(): Observable<Order[]> {
    return this.http.get<Order[]>(this.ordersUrl);
  }

  completeOrder(order: Order): Observable<Boolean> {
    return this.http.post<Boolean>(
      'http://localhost:8080/api/orders/complete',
      order
    );
  }

  placeOrder(order: Order): Observable<Order> {
    return this.http.post<Order>('http://localhost:8080/api/orders', order);
  }

  private handleError(res: HttpErrorResponse | any) {
    console.error(res.error || res.body.error);
    return observableThrowError(res.error || 'Server error');
  }
}
