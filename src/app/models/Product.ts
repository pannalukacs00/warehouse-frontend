export interface Product {
  id: number;
  name: string;
  quantity: number;
  contractor: string;
}
