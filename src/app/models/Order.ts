export interface Order {
  id: number;
  product: string;
  quantity: number;
  date: string;
  completed: boolean;
}
