import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Order } from '../models/Order';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css'],
})
export class OrdersComponent implements OnInit {
  orders: Order[];
  displayedColumns: string[];
  dataSource = new MatTableDataSource<Order>([]);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private orderService: OrderService) {}

  getOrders(): void {
    this.orderService.getOrders().subscribe(
      (response: Order[]) => {
        this.orders = response;
        this.dataSource.data = this.orders;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  onComplete(order: Order): void {
    this.orderService.completeOrder(order).subscribe({
      next: (_) => {
        order.completed = true;
      },
      error: (_) => {
        window.alert('Insufficient quantity in inventory!');
      },
    });
  }

  addItemToDataSource(newOrder: Order) {
    this.orders.push(newOrder);
    this.dataSource.data = this.orders;
  }

  ngOnInit(): void {
    this.displayedColumns = [
      'id',
      'product',
      'quantity',
      'date',
      'completed',
      'complete',
    ];
    this.getOrders();
    this.dataSource.paginator = this.paginator;
  }
}
