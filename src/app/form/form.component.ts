import { formatDate } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Order } from '../models/Order';
import { Product } from '../models/Product';
import { OrderService } from '../services/order.service';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  product = new FormControl();
  productsList: string[] = [];

  @Output() newItemEvent = new EventEmitter<Order>();

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private orderService: OrderService
  ) {}

  getProducts(): void {
    this.productService.getProducts().subscribe(
      (response: Product[]) => {
        var products = response;
        products.forEach((product) => {
          this.productsList.push(product.name);
        });
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      product: [null],
      quantity: [null],
    });
    this.getProducts();
  }

  orderProduct(form: any): void {
    let order = {} as Order;
    order.product = form.value.product;
    order.quantity = form.value.quantity;
    order.date = formatDate(Date.now(), 'yyyy-MM-dd', 'en-US');
    order.completed = false;
    console.log(order);

    this.orderService.placeOrder(order).subscribe(
      (response: Order) => {
        this.newItemEvent.emit(response);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );

    form.reset();
  }
}
