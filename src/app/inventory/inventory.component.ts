import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { Product } from '../models/Product';
import { HttpErrorResponse } from '@angular/common/http';
import { NONE_TYPE } from '@angular/compiler';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css'],
})
export class InventoryComponent implements OnInit {
  products: Product[];
  displayedColumns: string[];
  error: any;
  shouldShowTextbox: Boolean;
  productToModify: any;
  btnVal: string = 'Modify';

  constructor(private productService: ProductService) {}

  getProducts(): void {
    this.productService.getProducts().subscribe(
      (response: Product[]) => {
        this.products = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  onModify(event: any, product: Product): void {
    if (event.target.innerText === 'Save') {
      this.productService.modifyProduct(this.productToModify).subscribe({
        next: (_) => {},
        error: (_) => {
          window.alert('Could not update resource!');
        },
      });
      event.target.innerText = 'Modify';
      this.shouldShowTextbox = false;
      this.productToModify = null;
    } else {
      this.shouldShowTextbox = true;
      event.target.innerText = 'Save';
      this.productToModify = product;
    }
  }

  changeValue(event: any): void {
    this.productToModify.quantity = event.target.value;
  }

  ngOnInit(): void {
    this.displayedColumns = ['id', 'name', 'quantity', 'contractor', 'modify'];
    this.getProducts();
  }
}
